# -*- coding: utf-8 -*-
"""
Created on Fri Sep 18 10:46:33 2020

@author: martin
"""

class ABR:
    
    def __init__(self):
        self.Valeur = None
        self.Gauche = None
        self.Droit = None
    
    def __del__(self):
        None
    
    def SetValeur(self, V):
        self.Valeur = V
    
    def GetValeur(self):
        return self.Valeur
    
    def CreeGauche(self):
        self.Gauche = ABR()
    
    def CreeDroit(self):
        self.Droit = ABR()
    
    def Contient(self,k):
        if self.GetValeur() == None:
            return False
        else:
            if self.GetValeur() == k:
                return True
            elif self.GetValeur() > k:
                if self.Droit != None:
                    return self.Droit.Contient(k)
                else: return False
            elif self.GetValeur() < k:
                if self.Gauche != None:
                    return self.Gauche.Contient(k)
                else: return False
                
    def Maxi(self):
        if self.Droit == None:
            return self
        else:
            return self.Droit.Maxi()
                    
    def Mini(self):
        if self.Gauche == None:
            return self
        else:
            return self.Gauche.Mini()
    
    def Chercher(self,k):
        if self.GetValeur() == None:
            return False
        else:
            if self.GetValeur() == k:
                return self
            elif self.GetValeur() > k:
                if self.Droit != None:
                    return self.Droit.Contient(k)
                else: return False
            elif self.GetValeur() < k:
                if self.Gauche != None:
                    return self.Gauche.Contient(k)
                else: return False
            
    def Ajouter(self, k):
        if self.GetValeur() == None:
            print("k ajouté")
            self.SetValeur(k)
        else:
            if self.GetValeur() == k:
                None
            elif self.GetValeur() < k:
                print("k > V")
                if self.Gauche == None:
                    print("Gauche crée")
                    self.CreeGauche()
                self.Gauche.Ajouter(k)
            elif self.GetValeur() > k:
                print("k < V")
                if self.Droit == None:
                    print("Droit crée")
                    self.CreeDroit()
                self.Droit.Ajouter(k)
    
    def Supprimer(self, k):
        Fils = self.Chercher(k)
        if Fils == False:
            return self
        elif Fils != False:
            if (Fils.Gauche == None) and (Fils.Droit == None):
                Fils = ABR()
            elif (Fils.Gauche == None) and  (Fils.Droit != None):
                Fils = Fils.Droit
            elif (Fils.Gauche != None) and (Fils.Droit == None):
                Fils = Fils.Gauche   
            else:
                Max = Fils.Maxi()
                Fils = Max.GetValeur()
                Max = ABR()
                
            
