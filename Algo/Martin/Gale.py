def isin(a,L):
    for i in L:
        if i==a: return True
    return False

def GaleShapeley(H,F):
    from copy import deepcopy
    '''H et F des listes de liste de taille n*n. avec la ligne i des matrices représentant les préférences de plus a moins. 
    La fonction construit une matrice C 2*n des couples stables trouvés par l'algorythme'''
    Couples = [[],[]]
    Hommes = deepcopy(F)
    Femmes = deepcopy(F)
    n = len(Hommes)
    if len(Femmes) != n: return "#H =! #F" 
    while len(Couples[0])<n:
        for i in range(n):
            if not isin(i,Couples[0]):
                print(i, "propose a", Hommes[i][0])
                if not isin(Hommes[i][0],Couples[1]):
                    Couples[0].append(i)
                    Couples[1].append(Hommes[i][0])
                    print(i, "et",Hommes[i][0], "sont en couple")
                    Hommes[i].pop(0)
                
                else:
                    index_couple = Couples[1].index(Hommes[i][0])
                    rang_pretendant = Femmes[Hommes[i][0]].index(i)
                    rang_mari = Femmes[Hommes[i][0]].index(index_couple)
                    if rang_mari > rang_pretendant:
                        index_pretendant = Couples[0][index_couple]
                        Couples[0].pop(index_couple)
                        Couples[1].pop(index_couple)
                        Couples[0].append(i)
                        Couples[1].append(Hommes[i][0])
                        print(i, "et",Hommes[i][0], "sont en couple")
                        print(index_pretendant, "redeviens celibataire")
                        Hommes[i].pop(0)
                    else:
                        print(Hommes[i][0], "prefere", Couples[0][index_couple])
                        print(i, "reste celibataire")
                        Hommes[i].pop(0)
                print("les couples sont :")
                print(Couples)
                print("-----------")
                print("-----------")
    return Couples    