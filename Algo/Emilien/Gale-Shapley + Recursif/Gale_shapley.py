A = [1, 2 ,3]
B = [1, 2, 3]
Pref = [[2, 1, 3], # 1a préfère 2b puis 1b puis 3b
        [1, 3, 2],
        [1, 2, 3]]

Class = [[1, 3, 2], #pour 1b: 1a est premier, 2a troisième, 3a deuxième
        [2, 3, 1],
        [3, 2, 1]]

def GS(A, B, Pref, Class):
    A_free = A[:] #crée une copie modifiable de A
    B_free = B[:]
    demande = len(A)*[0] #indique l'élèment de B voulu par l'élèment de A
    couple = [[],[]]
    while A_free != []:
        n = len(A_free)-1 
        a_courant = A_free[n] #a_courant dernier élèment de A_free
        b_courant = Pref[n][demande[n]]
        A_free.remove(a_courant)

        if b_courant in B_free:
            couple[0].append(a_courant)
            couple[1].append(b_courant)
            B_free.remove(b_courant)
            demande[n] += 1

        else:
            index = couple[1].index(b_courant)
            a_prime = couple[0][index]
            i = B.index(b_courant)
            j = A.index(a_prime)

            if Class[i][n] > Class[i][j]:
                couple[0][index] = a_courant
                A_free.append(a_prime)
                demande[n] += 1

    return couple

print(GS(A, B, Pref, Class))